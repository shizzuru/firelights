package redlight

import (
	"time"
)

// NewRedlight is the redlight constructor
func NewRedlight(initTime time.Time) *Redlight {
	return &Redlight{
		initTime: initTime,
	}
}

// AddPattern add the provided patterns to the redlight object
func (r *Redlight) AddPattern(patterns ...LightsPattern) {
	for _, p := range patterns {
		r.pattern = append(r.pattern, p...)
	}
	r.updateTotalPatternDuration()
}

//SetPattern change the current pattern to the provided one.
func (r *Redlight) SetPattern(pattern LightsPattern) {
	r.pattern = pattern
	r.updateTotalPatternDuration()
}

// ResetTime resets the initTime to time.Now() so the lights loop starts over
func (r *Redlight) ResetTime() {
	r.initTime = time.Now()
}

// Status returns many useful informations to orchestrate other redlights around
func (r Redlight) Status() {

}

func (r *Redlight) updateTotalPatternDuration() {
	r.totalPatternDuration = func() (psum int) {
		for _, v := range r.pattern {
			psum += v[1]
		}
		return
	}()
}

// CurrentColors must not be exported
func (r Redlight) CurrentColors() int {
	runtime := int(time.Now().Sub(r.initTime).Seconds()) % r.totalPatternDuration
	var i int
	for _, v := range r.pattern {
		i += v[1]
		if i >= runtime {
			return v[0]
		}
	}
	return 0
}

// DebugPattern a
func (r Redlight) DebugPattern() LightsPattern {
	return r.pattern
}
