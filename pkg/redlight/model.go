package redlight

import "time"

// Redlight Colors
const (
	Red = 1 << iota
	Orange
	Green
)

// Redlight is the redlight DUH
type Redlight struct {
	initTime             time.Time
	pattern              LightsPattern
	totalPatternDuration int
}

// LightsPattern enables you to define any combination of colors/durations for your redlight.
// [redlight.Red | redlight.Orange, 5] would lit the red & orange lights for 5 seconds.
type LightsPattern [][]int

// Status is the datastructure used to orchestrate crossroads
type Status struct {
	CurrentColor    int
	TimeBeforeColor LightsPattern
	TimeOnColor     LightsPattern
}
