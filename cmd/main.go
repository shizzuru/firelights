package main

import (
	"fmt"
	"time"

	"gitlab.com/shizzuru/firelights/pkg/redlight"
)

func main() {
	r := redlight.NewRedlight(time.Now())
	r.SetPattern(redlight.LightsPattern{
		[]int{redlight.Red, 5},
		[]int{redlight.Green, 2},
		[]int{redlight.Orange, 3},
	})

	fmt.Printf("%v\n", r.DebugPattern())

	addP := redlight.LightsPattern{
		[]int{redlight.Green | redlight.Red, 10},
	}
	r.AddPattern(addP)

	fmt.Printf("%v\n", r.DebugPattern())
	for true {
		fmt.Printf("%v\n", r.CurrentColors())
	}
}
